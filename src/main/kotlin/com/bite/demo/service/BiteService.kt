package com.bite.demo.service

import com.bite.demo.model.BiteOrderRequest
import com.bite.demo.model.BiteOrderResponse
import org.springframework.stereotype.Service

@Service
class BiteService (){

    suspend fun postOrder(biteOrderRequest: BiteOrderRequest, orderId: String): BiteOrderResponse{
        return BiteOrderResponse(success = true, orderId = orderId)
    }
}