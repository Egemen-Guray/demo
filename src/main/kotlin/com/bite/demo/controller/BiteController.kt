package com.bite.demo.controller

import com.bite.demo.model.BiteOrderRequest
import com.bite.demo.model.BiteOrderResponse
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import com.bite.demo.service.BiteService

@RestController
@RequestMapping(path = ["/test"])
class BiteController(
    val biteService: BiteService
) {
    @PostMapping("/order/{orderID}")
    suspend fun postOrder(@PathVariable orderID: String,
                          @RequestBody biteOrderRequest: BiteOrderRequest): BiteOrderResponse {
        return biteService.postOrder(biteOrderRequest, orderID)
    }

    @PostMapping("/hello")
    suspend fun test(): String {
        return "hello world"
    }



}


