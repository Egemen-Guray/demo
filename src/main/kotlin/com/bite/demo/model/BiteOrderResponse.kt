package com.bite.demo.model

data class BiteOrderResponse (
    val success: Boolean,
    val orderId: String
)



