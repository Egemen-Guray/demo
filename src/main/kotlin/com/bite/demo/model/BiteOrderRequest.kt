package com.bite.demo.model

data class BiteOrderRequest (
    val items: Array<Int>,
    val total: Number,
)
